﻿/*
 * kisaMetaka jQuery plugin v0.3.1
 */
(function($) {

    $.fn.kisaMetaka = function(options) {        
        var canvas = $(this).get(0),
            isRunning = false,
            interval,
            snd = new Audio();
            
        var settings = {
            numberOfDrops:  50,
            speed:          10  
        }
        $.extend(settings, options);
        
        (function setupSound() {
            snd.loop = true;

            if (settings.sound) {
                if (snd.canPlayType("audio/ogg")) { 
                    snd.src = settings.sound + ".ogg"; 
                } else if (snd.canPlayType("audio/mp3")) { 
                    snd.src = settings.sound + ".mp3";
                }        
            }
        })();        
        
        var toggleMakeItRain = function() {
            var ctx,
                fallingDrops = [];

            $(canvas)
                .attr('width', $(window).width() )
                .attr('height', $(window).height() );
            
            $(window).resize(function respondCanvas() {
                $(canvas)
                    .attr('width', $(window).width() )
                    .attr('height', $(window).height() );
            });

            var draw = function() {
                ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
            
                for (var i = 0; i < settings.numberOfDrops; i++) {
                    ctx.drawImage (fallingDrops[i].image, fallingDrops[i].x, fallingDrops[i].y);
        
                    fallingDrops[i].y += fallingDrops[i].speed;
                    if (fallingDrops[i].y > ctx.canvas.height) {
                        fallingDrops[i].y = -25
                        fallingDrops[i].x = Math.random() * ctx.canvas.width;
                    }                
                }
            };
            
            if (canvas.getContext) {
                ctx = canvas.getContext('2d');
                
                for (var i = 0; i < settings.numberOfDrops; i++) {
                    var fallingDr = {};

                    fallingDr.image =  new Image();
                    fallingDr.image.src = settings.dropImage;                                
                    fallingDr.x = Math.random() * ctx.canvas.width;
                    fallingDr.y = Math.random() * ctx.canvas.height;
                    fallingDr.speed = settings.speed + Math.random() * settings.speed;
                    fallingDrops.push(fallingDr);
                } 
                
                if (!isRunning) {
                    $(canvas).show();
                    interval = setInterval(draw, 35);                    
                    if (snd.src) { snd.play(); }
                    isRunning = true;
                } else {
                    $(canvas).hide();
                    clearInterval(interval);                    
                    if (snd.src) { snd.pause(); }
                    isRunning = false;
                }              
            }            
        };

        (function attachRain() {
            var keyMap = [];

            var areKeysPressed = function() {
                var counter = 0;
                for (var i = 0; i < settings.keys.length; i++) {
                    if (keyMap[settings.keys[i]]) { counter++; }    
                }

                return (counter === settings.keys.length) ? true : false;
            }

            var keyFunc = function (e) {
                e = e || event;

                keyMap[e.keyCode] = e.type == 'keydown' ? true : false;

                if (areKeysPressed()) {
                    toggleMakeItRain();
                }
            };

            $(document).keydown(keyFunc);
            $(document).keyup(keyFunc);
        })();

        return this;
    }

})(jQuery || {});